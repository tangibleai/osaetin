// import { createWebHistory, createRouter } from "vue-router";
import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from "@/views/Home.vue";
import About from "@/views/About.vue";

// const routes = [
//   {
//     path: "/",
//     name: "Home",
//     component: Home,
//   },
//   {
//     path: "/about",
//     name: "About",
//     component: About,
//   },
// ];

// const router = createRouter({
//   history: createWebHistory(),
//   routes,
// });

// export default router;


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  // {
  //   path: '/boilerplate',
  //   name: 'Boilerplate Home',
  //   component: BoilerplateHome
  // },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: About,
    // component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  // {
  //   path: "/simple",
  //   name: 'Simple',
  //   component: SimpleComponent
  // },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
